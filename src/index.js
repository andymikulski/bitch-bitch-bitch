export default class BitchBitchBitch {
	constructor(verbose) {
		this.setup(!!verbose);
	}

	setup(verbose) {
		const self = this;
		self.verbose = verbose;
		if (self.init) {
			return;
		}
		self.init = true;
		console.originalLog = console.log;
		console.originalWarn = console.warn;
		console.originalError = console.error;

		console.log = function(){
			const args = arguments;
			self.speak(args, function(){
				console.originalLog.apply(console, args);
			});
		};
		console.warn = function(){
			const args = arguments;
			self.speak(args, function(){
				console.originalWarn.apply(console, args);
			});
		};
		console.error = function(){
			const args = arguments;
			self.speak(args, function(){
				console.originalError.apply(console, args);
			});
		};
	}


	speak(phrase, andThen) {
		var self = this;
		Array.prototype.forEach.call(phrase, function(a){
			// if we're verbose,
			// we'll read anything
			if (self.verbose) {
				window.speechSynthesis.speak(new SpeechSynthesisUtterance(a));
			} else {
				// if we're not,
				// we'll only read strings
				if(typeof a === 'string') {
					window.speechSynthesis.speak(new SpeechSynthesisUtterance(a));
				}
			}
		});
		if(andThen){
			andThen();
		}
	}
}