'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BitchBitchBitch = function () {
	function BitchBitchBitch(verbose) {
		_classCallCheck(this, BitchBitchBitch);

		this.setup(!!verbose);
	}

	_createClass(BitchBitchBitch, [{
		key: 'setup',
		value: function setup(verbose) {
			var self = this;
			self.verbose = verbose;
			if (self.init) {
				return;
			}
			self.init = true;
			console.originalLog = console.log;
			console.originalWarn = console.warn;
			console.originalError = console.error;

			console.log = function () {
				var args = arguments;
				self.speak(args, function () {
					console.originalLog.apply(console, args);
				});
			};
			console.warn = function () {
				var args = arguments;
				self.speak(args, function () {
					console.originalWarn.apply(console, args);
				});
			};
			console.error = function () {
				var args = arguments;
				self.speak(args, function () {
					console.originalError.apply(console, args);
				});
			};
		}
	}, {
		key: 'speak',
		value: function speak(phrase, andThen) {
			var self = this;
			Array.prototype.forEach.call(phrase, function (a) {
				// if we're verbose,
				// we'll read anything
				if (self.verbose) {
					window.speechSynthesis.speak(new SpeechSynthesisUtterance(a));
				} else {
					// if we're not,
					// we'll only read strings
					if (typeof a === 'string') {
						window.speechSynthesis.speak(new SpeechSynthesisUtterance(a));
					}
				}
			});
			if (andThen) {
				andThen();
			}
		}
	}]);

	return BitchBitchBitch;
}();

exports.default = BitchBitchBitch;