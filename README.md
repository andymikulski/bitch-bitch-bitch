## bitch-bitch-bitch

https://www.youtube.com/watch?v=ujBjn1qeCLs

Install with NPM:

```
sudo npm install bitch-bitch-bitch --save
```


Then import this somewhere early in your app (main.js, app.js, something like that):
```
import bitch from 'bitch-bitch-bitch';
new bitch();
```

In some cases, you may want verbose mode, in which the prompt attempts to read non-string arguments
```
// verbose mode:
new bitch(true);
```

https://www.youtube.com/watch?v=4IdzNTuVoMg#t=30s


### License
The MIT License (MIT)

Copyright (c) 2016 Andy Mikulski

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.